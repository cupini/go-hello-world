package main

import (
	"fmt"
)

func hello() string {
	return "Hello, Testing!"
}

func reverseHello() string {
	s := hello()
  r := []rune(s)
  for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
    r[i], r[j] = r[j], r[i]
  }
  return string(r)
}

func main() {
	fmt.Println(hello())
}
