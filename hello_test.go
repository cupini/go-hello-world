package main

import (
	"testing"
)

func TestHello(t *testing.T) {
	expectedStr := "Hello, Testing!"
	result := hello()
	if result != expectedStr {
		t.Fatalf("Expected %s, got %s", expectedStr, result)
	}
}

func TestReverseHello(t *testing.T) {
	expectedStr := "!gnitseT ,olleH"
	result := reverseHello()
	if result != expectedStr {
		t.Fatalf("Expected %s, got %s", expectedStr, result)
	}
}
